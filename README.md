# WenigZeit

A small UI application to manage my free slots for the calendar to run routine through the week.
Idea to use XP technic and mix the main working hours with routines like: sport, reading, health, accounting, parctices.

## Idea

There is a timeline of the calendar events. Everyday is not the same as yesterday.
Build a strategy to plan routine tasks (the regular duties) through the day.

It happens that you started a day a bit late, and your morning routines canceled.
Make sure their are finished, specify the time range when it could be done and mark that you are ready
to start the day.


```
Start a day           Work          Lunch                    18:00
│                   │                                       │                             │
│                   │ 30 min Work     30 min Work           │             Dinner          │                  End of the day
│                   │                                       │                             │
│    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │
───┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────┼────►   Time
│    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │    │
│                   │       15 min Routine      10 min Sport│                             │
│                   │                                       │                             │
│                   │     Daily routines                    │   Evening routines          │     Night routines
│ Morning routines  │  Mix the working hours                │                             │
with routines:                                                         Plan next day
Sport,               specify the interval like Pomodoro       Read book,
Breakfast            and have a list of routines for the      check children's homework
range. See the plan and change the       Walk outside to reach Steps
order. After accomplish confirm or       Plan your vacation
postpone the routine                     Workouts
Walk during that time to have Steps


Routines: Every day
Weekly
Specific day
Monthly
```


## Similar

- Calendar
- Habitica: Gamified Taskmanager (the most beautiful app that I found). It is like TODO list with perks.
- Habitat
- Routinery: Ritual/Routine
- Habit List

The main difference the routines is not separated as a huge time slot, but integrated to the flow and would give some time to rest from work.
It checks the current calendar events and tries to schedule the activity to the best time.
It would also notify if there is too much scheduled for today.
Idea of my planner to find a slot and it could be work with any of apps to pick a correct routine.

## Implementation

1. Show current events similar to calendar list view
1. Create routine
  1. Title, Duration, Range of possible hours, Frequency
1. Settings page
  1. About page

# References

- [Swift: Statements](https://docs.swift.org/swift-book/ReferenceManual/Statements.html)
- [Guard Statement Explained in Swift](https://learnappmaking.com/swift-guard-let-statement-how-to/)
- [Swift: Concurrency](https://docs.swift.org/swift-book/LanguageGuide/Concurrency.html)
- [async/await in SwiftUI](https://www.raywenderlich.com/25013447-async-await-in-swiftui)
- [iOS Design : Human interface guidelines : Modality](https://developer.apple.com/design/human-interface-guidelines/ios/app-architecture/modality/)
- [SwiftUI for iOS 15: How to Add Swipe Actions and Hide Line Separators in List View](https://www.appcoda.com/swiftui-swipe-actions/)
- [The Complete Guide to NavigationView in SwiftUI](https://www.youtube.com/watch?v=nA6Jo6YnL9g)
- [Fun with print() in Swift ](https://learnappmaking.com/print-swift-how-to/)
- [How to Read and Cancel Local Notification Requests](https://www.appsdeveloperblog.com/how-to-read-and-cancel-local-notification-requests/)
- [Triggering events repeatedly using a timer](https://www.hackingwithswift.com/books/ios-swiftui/triggering-events-repeatedly-using-a-timer)
- [Pull-to-Refresh in SwiftUI](https://swiftwithmajid.com/2021/07/14/pull-to-refresh-in-swiftui/)
- [SwiftUI: List](https://developer.apple.com/documentation/swiftui/list)
- [Xcode Keyboard Shortcuts and Gestures](https://developer.apple.com/library/archive/documentation/IDEs/Conceptual/xcode_help-command_shortcuts/MenuCommands/MenuCommands014.html)
- [14 Xcode Keyboard Shortcuts - To Make You More a Productive iOS
   Developer](https://supereasyapps.com/blog/2014/9/15/14-xcode-time-saving-shortcuts-memorize-and-improve-your-productivity)

  - `Command + Shift + O`: Open quick
  - `Command + Shift + J`: reveals the file you currently have open in the project navigator
  - `Command + Shift + F`: Search
  - `Control + 6`: File Jump Ba

- [Initializers & init() Explained in Swift](https://learnappmaking.com/initializers-init-swift-how-to/)
- [Relative date time formatting in SwiftUI](https://noahgilmore.com/blog/swiftui-relativedatetimeformatter/)
- [How to format text inside text views](https://www.hackingwithswift.com/quick-start/swiftui/how-to-format-text-inside-text-views)
- [Drag and Drop Editable Lists: Tutorial for SwiftUI](https://www.raywenderlich.com/22408716-drag-and-drop-editable-lists-tutorial-for-swiftui#toc-anchor-005)
- [How to make SwiftUI NavigationLink work in edit mode?](https://stackoverflow.com/questions/58236010/how-to-make-swiftui-navigationlink-work-in-edit-mode)
- [Presenting Sheets in SwiftUI](https://serialcoder.dev/text-tutorials/swiftui/presenting-sheets-in-swiftui/)
