//
//  routines_edit.swift
//  WenigZeit
//
//  Created by Michael Nikitochkin on 19.09.21.
//

import SwiftUI

struct RoutinesList: View {

  @EnvironmentObject var context:Context
  @State var selected_routine: String? = nil

  private func delete(atOffsets offsets: IndexSet) {
    context.routines.remove(atOffsets: offsets)
  }

  private func move(fromOffsets from: IndexSet, toOffset to: Int) {
    context.routines.move(fromOffsets: from, toOffset: to)
  }

  @State var show_routine_add_modal = false

  var body: some View {
    List {
      ForEach(context.routines) { routine in
        NavigationLink(
          destination: RoutineEdit(routine: routine),
          tag: routine.id,
          selection: self.$selected_routine
        ) {
          Text(routine.description)
        }
        .onTapGesture(perform: { self.selected_routine = routine.id })
      }
      .onDelete(perform: delete)
      .onMove(perform: move(fromOffsets:toOffset:))
    }
    .environment(\.editMode, .constant(EditMode.active))
    .navigationTitle("Routines")
    .navigationBarItems(
      trailing: Button(action: {
        self.show_routine_add_modal = true
      } ) {
        Image(systemName: "plus")
      }
    )
    .sheet(isPresented: $show_routine_add_modal, content: {
      RoutineCreate()
    })
  }
}

struct RoutinesList_Previews: PreviewProvider {
  static var previews: some View {
    RoutinesList()
      .environmentObject(Context())
  }
}

