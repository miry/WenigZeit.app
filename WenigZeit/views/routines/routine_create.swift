//
//  routine_edit.swift
//  WenigZeit
//
//  Created by Michael Nikitochkin on 19.09.21.
//

import SwiftUI

struct RoutineCreate: View {
  @EnvironmentObject
  var context:Context

  @Environment(\.presentationMode)
  var presentationMode: Binding<PresentationMode>

  @State
  var valid = true
  @State
  private var routine = Routine(title: "", duration_min: 5)

  var body: some View {
    NavigationView {
      Form {
        TextField(
          "Title",
          text: $routine.title
        )
        TextField(
          "Duration (min)",
          value: $routine.duration_min,
          formatter: NumberFormatter()
        )
      }
      .navigationBarTitle("New routine")
      .navigationBarTitleDisplayMode(.inline)
      .navigationBarItems(
        leading:
          Button("Cancel", action: {
            self.presentationMode.wrappedValue.dismiss()
          })
          .foregroundColor(.red)
        ,
        trailing:
          Button("Done", action: {
            context.routines.append(self.routine)
            self.presentationMode.wrappedValue.dismiss()
          })
          .disabled(!routine.valid())
      )
    }
  }
}

struct RoutineCreate_Previews: PreviewProvider {
  static var previews: some View {
    RoutineCreate()
      .environmentObject(Context())
  }
}
