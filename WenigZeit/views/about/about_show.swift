//
//  AboutShow.swift
//  WenigZeit
//
//  Created by Michael Nikitochkin on 19.09.21.
//

import SwiftUI

struct AboutShow: View {
  var body: some View {
    Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
  }
}

struct AboutShow_Previews: PreviewProvider {
  static var previews: some View {
    AboutShow()
  }
}
