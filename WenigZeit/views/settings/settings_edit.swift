//
//  settings.swift
//  WenigZeit
//
//  Created by Michael Nikitochkin on 16.09.21.
//

import SwiftUI
import EventKitUI

struct SettingsRowView: View {
  var title: String
  var iconName: String

  var body: some View {
    HStack {
      Image(systemName: iconName)
      Text (title)
    }
  }
}

struct SettingsEdit: View {

  @EnvironmentObject var context: Context

  @State private var start_working_date = Date()
  @State private var end_working_date = Date()

  var body: some View {
    NavigationView {
      VStack(alignment: .leading) {
        Form {
          Toggle("Notifications", isOn: $context.enable_reminders)
          DatePicker("Start working hours",
                     selection: $start_working_date,
                     displayedComponents: .hourAndMinute
          )
            .onChange(of: start_working_date) { newValue in
              if newValue >= context.end_working_date_raw {
                start_working_date = context.start_working_date_raw
              } else {
                let c = Foundation.Calendar.current.dateComponents([.hour, .minute], from: newValue)
                context.start_working_date.hour = c.hour!
                context.start_working_date.minute = c.minute!
              }
            }
          DatePicker("End working hours",
                     selection: $end_working_date,
                     displayedComponents: .hourAndMinute
          )
            .onChange(of: end_working_date) { newValue in
              if newValue <= context.start_working_date_raw {
                end_working_date = context.end_working_date_raw
              } else {
                let c = Foundation.Calendar.current.dateComponents([.hour, .minute], from: newValue)
                context.end_working_date.hour = c.hour!
                context.end_working_date.minute = c.minute!
              }
            }
        }

        List {
          NavigationLink(destination: CalendarsList(), label: {
            SettingsRowView(
              title: "Calendars",
              iconName: "calendar"
            )
          })

          NavigationLink(destination: RoutinesList(), label: {
            SettingsRowView(
              title: "Routines",
              iconName: "person"
            )
          })
        }

        Spacer()
      }
      .navigationTitle("Settings")
    }
    .onAppear {
      start_working_date = context.start_working_date_raw
      end_working_date = context.end_working_date_raw
    }
  }
}

struct SettingsEdit_Previews: PreviewProvider {
  static var previews: some View {
    SettingsEdit()
      .environmentObject(Context())
  }
}
