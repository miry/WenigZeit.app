//
//  ContentView.swift
//  WenigZeit
//
//  Created by Michael Nikitochkin on 13.09.21.
//

import SwiftUI

struct ContentView: View {

  @EnvironmentObject var context : Context

  private enum Tabs: Hashable {
    case events, settings
  }

  var body: some View {
    TabView {
      EventsList()
        .tabItem {
          Label("Events", systemImage: "list.bullet.rectangle")
        }
        .tag(Tabs.events)
      SettingsEdit()
        .tabItem {
          Label("Settings", systemImage: "gear")
        }
        .tag(Tabs.settings)
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
      .environmentObject(Context())
  }
}
